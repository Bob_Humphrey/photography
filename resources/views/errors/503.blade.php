@extends('app')

@section('pageTitle')

Sorry!

@stop

@section('pageClass') error-page @stop

@section('content')

<div class="error-message">
  The site is currently down for maintenance.  We will be right back!
</div>

@stop
