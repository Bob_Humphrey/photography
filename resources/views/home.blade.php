@extends('app')

@section('pageTitle')

Tumblr Photos

@stop

@section('pageClass') home-page @stop

@section('content')

<h3>Description</h3>

  <p>
    I created this application to see if I could find a wider audience for
    my photography and increase the number of followers of
    <a href="http://bobhumphreyphotography.tumblr.com/" target="_blank">
    my tumblr photography site</a>.
    There are several popular tumblr sites  that showcase the work of
    unknown photographers.  Many of these sites allow you to submit your
    work to them for consideration and possible posting.  I use this
    application to keep track of my submissions to these sites.
  </p>

<h3>Built With</h3>

  <p>PHP, Laravel, JavaScript, DropZone.js, MySQL, Bootstrap,
  and the Intervention image manipulation library.</p>

<h3>To Use</h3>

<p>From the main menu:</p>

<ul>
  <li>PHOTOS/DISPLAY - Displays each of the photos I've posted to Tumblr.</li>
  <li>PHOTOS/LIST - Displays a list of my photos, along with the numbe of "likes"
    each one has received.</li>
  <li>SITES - Displays a list of tumblr sites that post artistic photographs.</li>
  <li>REBLOGS - Lists photos I've submitted to other sites, and whether they
    were accepted for posting or reblogging.</li>
</ul>

<h3>Additional Admin Pages</h3>

  <p>Available only after logging in.</p><br>

  <div class="row">
    <div class='col-md-6'>
      <img src="images/tp1.jpg" class="img-responsive"/>
    </div>
    <div class='col-md-6'>
      <img src="images/tp2.jpg" class="img-responsive"/>
    </div>
  </div>

  <div class="row">
    <div class='col-md-6'>
      <img src="images/tp3.jpg" class="img-responsive"/>
    </div>
    <div class='col-md-6'>
      <img src="images/tp4.jpg" class="img-responsive"/>
    </div>
  </div>

</div>

@stop
